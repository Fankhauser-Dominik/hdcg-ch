import defaultImage from './assets/images/HDC_Logo.png';

const CONFIG = {
  name: 'HDC Gelterkinden',

  origin: 'https://hdcg.ch/',
  basePathname: '/',
  trailingSlash: false,

  title: 'HDC Gelterkinden — Offizielle Webseite',
  description: 'Seit 1982',
  defaultImage: defaultImage,

  defaultTheme: 'system', // Values: "system" | "light" | "dark" | "light:only" | "dark:only"

  language: 'de',
  textDirection: 'ltr',

  dateFormatter: new Intl.DateTimeFormat('de', {
    year: 'numeric',
    month: 'short',
    day: 'numeric',
    timeZone: 'UTC',
  }),

  googleAnalyticsId: false, // or "G-XXXXXXXXXX",
  googleSiteVerificationId: 'orcPxI47GSa-cRvY11tUe6iGg2IO_RPvnA1q95iEM3M',

  news: {
    disabled: false,
    postsPerPage: 10,

    post: {
      permalink: '/%slug%', // Variables: %slug%, %year%, %month%, %day%, %hour%, %minute%, %second%, %category%
      noindex: false,
      disabled: false,
    },

    list: {
      pathname: 'news', // News main path, you can change this to "articles" (/articles)
      noindex: false,
      disabled: false,
    },

    category: {
      pathname: 'category', // Category main path /category/some-category
      noindex: false,
      disabled: false,
    },

    tag: {
      pathname: 'tag', // Tag main path /tag/some-tag
      noindex: false,
      disabled: false,
    },
  },
};

export const SITE = { ...CONFIG, news: undefined };
export const NEWS = CONFIG.news;
export const DATE_FORMATTER = CONFIG.dateFormatter;
