import React from 'react';
import { Map, Marker, ZoomControl } from 'pigeon-maps';

export default function MyMap() {
  return (
    <Map height={600} defaultCenter={[47.467198, 7.8592]} defaultZoom={19}>
      <Marker
        color="red"
        anchor={[47.467198, 7.8592]}
        onClick={() => window.open('https://goo.gl/maps/66wfwgx176b58hxz7', '_blank')}
      />
      <ZoomControl />
    </Map>
  );
}
