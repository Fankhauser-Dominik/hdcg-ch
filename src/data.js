import { getPermalink, getNewsPermalink, getAsset } from './utils/permalinks';

export const headerData = {
  links: [
    {
      text: 'Verein',
      links: [
        {
          text: 'Über uns',
          href: '/club/about',
        },
        {
          text: 'Vereinslokal',
          href: '/club/clubhouse',
        },
        {
          text: 'Vorstand',
          href: '/club/board',
        },
      ],
    },
    // {
    //   text: 'Pages',
    //   links: [
    //     {
    //       text: 'Features',
    //       href: '#',
    //     },
    //     {
    //       text: 'Pricing',
    //       href: '#',
    //     },
    //     {
    //       text: 'About us',
    //       href: '#',
    //     },
    //     {
    //       text: 'Contact',
    //       href: '#',
    //     },
    //     {
    //       text: 'Terms',
    //       href: '/terms',
    //     },
    //     {
    //       text: 'Privacy policy',
    //       href: '/privacy',
    //     },
    //   ],
    // },
    {
      text: 'Gallerie',
      href: '/gallery',
    },
    {
      text: 'Kontakt',
      href: '/contact',
    },
    {
      text: 'News',
      href: getNewsPermalink(),
    },
  ],
  // actions: [{ type: 'button', text: 'Download', href: 'https://github.com/onwidget/astrowind' }],
};

export const footerData = {
  links: [
    {
      title: 'Product',
      links: [
        { text: 'Features', href: '#' },
        { text: 'Security', href: '#' },
        { text: 'Team', href: '#' },
        { text: 'Enterprise', href: '#' },
        { text: 'Customer stories', href: '#' },
        { text: 'Pricing', href: '#' },
        { text: 'Resources', href: '#' },
      ],
    },
    {
      title: 'Platform',
      links: [
        { text: 'Developer API', href: '#' },
        { text: 'Partners', href: '#' },
        { text: 'Atom', href: '#' },
        { text: 'Electron', href: '#' },
        { text: 'AstroWind Desktop', href: '#' },
      ],
    },
    {
      title: 'Support',
      links: [
        { text: 'Docs', href: '#' },
        { text: 'Community Forum', href: '#' },
        { text: 'Professional Services', href: '#' },
        { text: 'Skills', href: '#' },
        { text: 'Status', href: '#' },
      ],
    },
    {
      title: 'Company',
      links: [
        { text: 'About', href: '#' },
        { text: 'News', href: '#' },
        { text: 'Careers', href: '#' },
        { text: 'Press', href: '#' },
        { text: 'Inclusion', href: '#' },
        { text: 'Social Impact', href: '#' },
        { text: 'Shop', href: '#' },
      ],
    },
  ],
  secondaryLinks: [
    { text: 'Terms', href: getPermalink('/terms') },
    { text: 'Privacy Policy', href: getPermalink('/privacy') },
  ],
  socialLinks: [
    /* { ariaLabel: 'Twitter', icon: 'tabler:brand-twitter', href: '#' }, */
    { ariaLabel: 'Instagram', icon: 'tabler:brand-instagram', href: 'https://www.instagram.com/hdcgelterkinden/' },
    { ariaLabel: 'Facebook', icon: 'tabler:brand-facebook', href: 'https://www.facebook.com/HDCGelterkinden/' },
    { ariaLabel: 'RSS', icon: 'tabler:rss', href: getAsset('/rss') },
  ],
  footNote: `
    Made by <a class="text-blue-600 hover:underline dark:text-gray-200" href="https://fanki.dev/"> Dominik Fankhauser</a> · All rights reserved.
  `,
};

import actuaryImage from '~/assets/images/gallery/Players/Angela_Heinrich.jpg';
import presidentImage from '~/assets/images/gallery/Players/Dominik_Borer.jpg';
import cashierImage from '~/assets/images/gallery/Players/Marco_Lessa.jpg';
import boardMemberImage from '~/assets/images/gallery/Players/Lukas_Buser.jpg';
export const boardData = {
  teamNames: ['Vorstand'],
  players: [
    {
      name: 'Dominik Borer',
      team: 'Vorstand',
      role: { Vorstand: 'Präsident' },
      since: 2014,
      image: { src: presidentImage, alt: 'Dominik Borer' },
    },
    {
      name: 'Daniel Rhyn',
      team: 'Vorstand',
      role: { Vorstand: 'Vizepräsident' },
      since: 0,
      image: null,
    },
    {
      name: 'Angela Heinrich',
      team: 'Vorstand',
      role: { Vorstand: 'Aktuarin' },
      since: 2017,
      image: {
        src: actuaryImage,
        alt: 'Angela Heinrich',
      },
    },
    {
      name: 'Lukas Buser',
      team: 'Vorstand',
      role: { Vorstand: 'Beisizter' },
      since: 0,
      image: {
        src: boardMemberImage,
        alt: 'Lukas Buser',
      },
    },
    {
      name: 'Marco Lessa',
      team: 'Vorstand',
      role: { Vorstand: 'Kassier' },
      since: 0,
      image: {
        src: cashierImage,
        alt: 'Marco Lessa',
      },
    },
  ],
};
